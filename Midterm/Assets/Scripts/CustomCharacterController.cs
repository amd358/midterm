﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class CustomCharacterController : MonoBehaviour {

	public float maxSpeed;
	public float speed = 100f; // how fast the player 
	public float jumpForce = 150f;

	public bool grounded;

	private Rigidbody2D rigidBody;

	private Animator anim;


	void Start () {
		rigidBody = gameObject.GetComponent<Rigidbody2D> ();
		anim = gameObject.GetComponent<Animator> ();
	}


	void Update () { //Code updates in every single frame
		anim.SetBool ("IsGrounded", grounded);
		anim.SetFloat ("Speed", Mathf.Abs (rigidBody.velocity.x));


		float h = Input.GetAxis ("Horizontal");


		//Flipping character's sprite to face xorrect direction on x axis
		if (Input.GetAxis ("Horizontal") < -.001f) {
			transform.localScale = new Vector3 (-2.8f, 2.8f, 2.8f);
		}


		//Moving the character
		rigidBody.AddForce ((Vector2.right * speed) * h); // moves the player when you move left and right


		if (Input.GetAxis ("Horizontal") > .001f) { 
			transform.localScale = new Vector3 (2.8f, 2.8f, 2.8f);
		}


		if (Input.GetKeyDown (KeyCode.Space) && grounded) {
			rigidBody.AddForce (Vector2.up * jumpForce);
		}


}

	void FixedUpdate () { //Code updates every few frames for things that have a delay

		Vector3 easeVelocity = rigidBody.velocity;
		easeVelocity.y = rigidBody.velocity.y;
		easeVelocity.z = 0.0f;
		easeVelocity.x *= 0.75f;


		if (grounded) {
			rigidBody.velocity = easeVelocity;
		}


		//Limiting the speed of the character
		if (rigidBody.velocity.x > maxSpeed) {
			rigidBody.velocity = new Vector2 (maxSpeed, 0f);
		}


		if (rigidBody.velocity.x < -maxSpeed) {
			rigidBody.velocity = new Vector2 (-maxSpeed, 0f);
			}

	}

}