﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI; // give access to UI backend
using UnityStandardAssets._2D; // gives access to Standard 2D backend

public class HealthSystem : MonoBehaviour{

	public Sprite [] HeartSprites; //an array/list of heart sprites - why? Cause we wil be changing them according to the curHealth variable

	public Image HeartUI; // UI system = Canvas in Editor

	private PlatformerCharacter2D player; // Private access to the character controller script

	void Start() {

		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<UnityStandardAssets._2D.PlatformerCharacter2D> (); // Find the character controller and get the platformcharacter2D script

	}

	void update() {

		HeartUI.sprite = HeartSprites [player.curHealth]; // set heart sprite to current health value

	}



}
